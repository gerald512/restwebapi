
using Newtonsoft.Json;
using RestWebApi.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace RestWebApi
{
    public class CRUDOperations
    {
        public async Task<IEnumerable<Product>> GetProduct(long userId)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://192.168.43.242:61913/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var result = await client.GetAsync(String.Format("/api/Product?userId={0}", userId));

                if (result.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<IEnumerable<Product>>(await result.Content.ReadAsStringAsync());
                }
                else
                {
                    return null;
                }
            }
        }

        public async Task AddProduct(Product product)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://192.168.43.242:61913/"); // < - TU OCZYWISCIE TWOJE IP, LUB LOCALHOST Z PORTEM

                var json = JsonConvert.SerializeObject(product);

                var content = new StringContent(json, Encoding.UTF8, "application/json");

                var response = await client.PostAsync("/api/Product", content);
            }
        }        

        public async Task DeleteProduct(long id)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://192.168.43.242:61913/");

                var response = await client.DeleteAsync(String.Format("/api/Product/{0}", id));

            }
        }        

        public async Task UpdateProduct(Product product)
        {
            using (var client = new HttpClient())
            {

                client.BaseAddress = new Uri("http://192.168.43.242:61913/");

                var json = JsonConvert.SerializeObject(product);

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PutAsync("/api/Product", content);
            }
        }
    }
}