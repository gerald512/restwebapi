﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using RestWebApi.Models;

namespace RestWebApi
{
    public class ProductPersistence
    {
        private MySqlConnection conn;

        public ProductPersistence()
        {
            string myConnectionString;
            myConnectionString = "server=mysql3.gear.host;database=onlinefridge;uid=onlinefridge;password=lodowka123#;Convert Zero Datetime=True";
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                //
            }
        }

        public List<Product> Get()
        {
            var productsList = new List<Product>();
            string sqlString = "SELECT * FROM products";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                productsList.Add(new Product
                {
                    productId = dataReader.GetInt32(0),
                    name = dataReader.GetString(1),
                    count = dataReader.GetInt32(2),
                    unit = (Quantity)dataReader.GetInt32(3),
                    expDate = dataReader.GetDateTime(dataReader.GetOrdinal("ExpirationDate")).ToString("yyyy-MM-dd"),
                    userId = dataReader.GetInt32(5),
                    inShoppingList = dataReader.GetBoolean(6)
                });
            }
            return productsList;
        }

        public Product GetPerson(int Id)
        {
            Product p = new Product();
            MySqlDataReader mySqlReader = null;

            string sqlString = "SELECT * FROM products WHERE ProductId = " + Id.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                p.productId = mySqlReader.GetInt32(0);
                p.name = mySqlReader.GetString(1);
                p.count = mySqlReader.GetInt32(2);
                p.unit = (Quantity)mySqlReader.GetInt32(3);
                p.expDate = mySqlReader.GetString(4);
                p.userId = mySqlReader.GetInt32(5);
                p.inShoppingList = mySqlReader.GetBoolean(6);

                return p;
            }
            else
            {
                return null;
            }
        }

        public List<Product> GetPerson(long userId)
        {
            List<Product> products = new List<Product>();
            MySqlDataReader dataReader = null;

            string sqlString = "SELECT * FROM products WHERE UserId = " + userId.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                products.Add(new Product
                {
                    productId = dataReader.GetInt32(0),
                    name = dataReader.GetString(1),
                    count = dataReader.GetInt32(2),
                    unit = (Quantity)dataReader.GetInt32(3),
                    expDate = dataReader.GetDateTime(dataReader.GetOrdinal("ExpirationDate")).ToString("yyyy-MM-dd"),
                    userId = dataReader.GetInt32(5),
                    inShoppingList = dataReader.GetBoolean(6)
                });
            }
            return products;
        }


        public bool UpdateProduct(Product ProductToSave)
        {
            MySqlDataReader mySqlReader = null;

            string sqlString = "SELECT * FROM products WHERE ProductId = " + ProductToSave.productId.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            var expirationDate = new DateTime();
            if (!String.IsNullOrWhiteSpace(ProductToSave.expDate))
            {
                expirationDate = DateTime.Parse(ProductToSave.expDate);
            }

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                mySqlReader.Close();

                sqlString = "UPDATE products SET Type='" + ProductToSave.name + "', Count=" + ProductToSave.count +
                            ", TypeOfCountList=" + (int)ProductToSave.unit + ", ExpirationDate='" + expirationDate +
                            "', UserId=" + ProductToSave.userId + ", InShoppingList=" + ProductToSave.inShoppingList +
                            " WHERE ProductId =" + ProductToSave.productId.ToString();
                cmd = new MySqlCommand(sqlString, conn);

                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteProduct(long Id)
        {
            Product p = new Product();
            MySqlDataReader mySqlReader = null;

            string sqlString = "DELETE FROM products WHERE ProductId = " + Id.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                mySqlReader.Close();

                sqlString = "DELETE FROM products WHERE ProductId = " + Id.ToString();
                cmd = new MySqlCommand(sqlString, conn);

                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public long SaveProduct(Product ProductToSave)
        {
            var expirationDate = new DateTime();
            if (!String.IsNullOrWhiteSpace(ProductToSave.expDate))
            {
                expirationDate = DateTime.Parse(ProductToSave.expDate);
            }
            string sqlString = "INSERT INTO products (Type, Count, TypeOfCountList, ExpirationDate, UserId, InShoppingList) VALUES ('" + ProductToSave.name + "'," + ProductToSave.count + "," + (int)ProductToSave.unit + ",'" + expirationDate + "'," + ProductToSave.userId + "," + ProductToSave.inShoppingList + ")";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);
            cmd.ExecuteNonQuery();
            long id = cmd.LastInsertedId;
            return id;
        }
    }
}