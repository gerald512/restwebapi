﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestWebApi.Models;

namespace RestWebApi.Controllers
{
    public class UserController : ApiController
    {

        // GET: api/User
        public List<User> Get()
        {
            UserPersistence us = new UserPersistence();
            var userList = new List<User>();
            userList = us.Get();

            return userList;

        }

        /// <summary>
        /// Get a specific user by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        // GET: api/Product?email="kwiatopl@gmail.com"
        public User Get(string email)
        {
            UserPersistence us = new UserPersistence();
            User user = us.GetUser(email);

            return user;
        }

        // GET: api/User/5
        public User Get(int id)
        {
            UserPersistence us = new UserPersistence();
            User user = us.GetUser(id);

            return user;
        }

        // POST: api/User
        public HttpResponseMessage Post([FromBody]User value)
        {
            UserPersistence us = new UserPersistence();
            long id;
            id = us.SaveUser(value);
            value.userId = (int)id;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, string.Format("User/{0}", id));
            return response;
        }

        // PUT: api/User
        public HttpResponseMessage Put([FromBody]User value)
        {
            UserPersistence us = new UserPersistence();
            bool recordExisted = false;
            recordExisted = us.UpdateUser(value);

            HttpResponseMessage response;

            if (recordExisted)
            {
                response = Request.CreateResponse(HttpStatusCode.Created);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;
        }

        // DELETE: api/User/5
        public HttpResponseMessage Delete(long id)
        {
            UserPersistence us = new UserPersistence();
            HttpResponseMessage response;

            bool recordExisted = false;

            recordExisted = us.DeleteUser(id);

            if (recordExisted)
            {
                response = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;

        }
    }
}
