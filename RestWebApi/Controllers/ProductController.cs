﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using RestWebApi.Models;

namespace RestWebApi.Controllers
{
    public class ProductController : ApiController
    {
        
        // GET: api/Product
        public List<Product> Get()
        {
            ProductPersistence prod = new ProductPersistence();
            var productsList = new List<Product>();
            productsList = prod.Get();

            return productsList;

        }

        // GET: api/Product/5
        public Product Get(int id)
        {
            ProductPersistence prod = new ProductPersistence();
            Product product = prod.GetPerson(id);

            return product;
        }
        /// <summary>
        /// Get a specific product by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        // GET: api/Product?userId=1
        public List<Product> Get(long userId)
        {
            ProductPersistence prod = new ProductPersistence();
            List<Product> products = prod.GetPerson(userId);
            if (products == null)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
            }
            return products;
        }

        // POST: api/Product
        public HttpResponseMessage Post([FromBody]Product value)
        {
            ProductPersistence prod = new ProductPersistence();
            long id;
            id = prod.SaveProduct(value);
            value.productId = (int)id;
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
            response.Headers.Location = new Uri(Request.RequestUri, String.Format("Product/{0}",id));
            return response;
        }

        // PUT: api/Product
        public HttpResponseMessage Put([FromBody]Product value)
        {
            ProductPersistence prod = new ProductPersistence();
            bool recordExisted = false;
            recordExisted = prod.UpdateProduct(value);

            HttpResponseMessage response;

            if (recordExisted)
            {
                response = Request.CreateResponse(HttpStatusCode.Created);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;
        }

        // DELETE: api/Product/5
        public HttpResponseMessage Delete(long id)
        {
            ProductPersistence prod = new ProductPersistence();
            HttpResponseMessage response;
            
            bool recordExisted = false;

            recordExisted = prod.DeleteProduct(id);

            if (recordExisted)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }
            return response;

        }
    }
}
