﻿using System.Collections.Generic;
using MySql.Data.MySqlClient;
using RestWebApi.Models;

namespace RestWebApi
{
    public class UserPersistence
    {
        private MySqlConnection conn;

        public UserPersistence()
        {
            string myConnectionString;
            myConnectionString = "server=mysql3.gear.host;database=onlinefridge;uid=onlinefridge;password=lodowka123#;Convert Zero Datetime=True";
            try
            {
                conn = new MySqlConnection();
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }
            catch (MySqlException ex)
            {
                
            }
        }

        public List<User> Get()
        {
            var userList = new List<User>();
            string sqlString = "SELECT * FROM users";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            MySqlDataReader dataReader = cmd.ExecuteReader();

            while (dataReader.Read())
            {
                userList.Add(new User
                {
                    userId = dataReader.GetInt32(0),
                    email = dataReader.GetString(1),
                    password = dataReader.GetString(2),
                    passwordHash = dataReader.GetString(3),
                    salt = dataReader.GetString(4),
                    username = dataReader.GetString(5)

                });
            }
            return userList;
        }

        public User GetUser(string email)
        {
            User u = new User();
            MySqlDataReader mySqlReader = null;

            string sqlString = "SELECT * FROM users WHERE Email ='" + email + "'";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                u.userId = mySqlReader.GetInt32(0);
                u.email = mySqlReader.GetString(1);
                u.password = mySqlReader.GetString(2);
                u.passwordHash = mySqlReader.GetString(3);
                u.salt = mySqlReader.GetString(4);
                u.username = mySqlReader.GetString(5);

                return u;
            }
            else
            {
                return null;
            }
        }

        public User GetUser(int Id)
        {
            User u = new User();
            MySqlDataReader mySqlReader = null;

            string sqlString = "SELECT * FROM users WHERE Id = " + Id.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                u.userId = mySqlReader.GetInt32(0);
                u.email = mySqlReader.GetString(1);
                u.password = mySqlReader.GetString(2);
                u.passwordHash = mySqlReader.GetString(3);
                u.salt = mySqlReader.GetString(4);
                u.username = mySqlReader.GetString(5);

                return u;
            }
            else
            {
                return null;
            }
        }

        public bool UpdateUser(User UserToSave)
        {
            MySqlDataReader mySqlReader = null;

            string sqlString = "SELECT * FROM users WHERE Id = " + UserToSave.userId.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                mySqlReader.Close();

                sqlString = "UPDATE users SET Email='" + UserToSave.email + "', Password='" + UserToSave.password +
                            "', PasswordHash='" + UserToSave.passwordHash + "', Salt='" + UserToSave.salt + "', UserName='" + UserToSave.username +
                            "' WHERE Id =" + UserToSave.userId.ToString();

                cmd = new MySqlCommand(sqlString, conn);

                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool DeleteUser(long Id)
        {
            User u = new User();
            MySqlDataReader mySqlReader = null;

            string sqlString = "DELETE FROM users WHERE Id = " + Id.ToString();
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);

            mySqlReader = cmd.ExecuteReader();
            if (mySqlReader.Read())
            {
                mySqlReader.Close();

                sqlString = "DELETE FROM users WHERE Id = " + Id.ToString();
                cmd = new MySqlCommand(sqlString, conn);

                cmd.ExecuteNonQuery();
                return true;
            }
            else
            {
                return false;
            }
        }

        public long SaveUser(User UserToSave)
        {
            string sqlString = "INSERT INTO users (Email, Password, PasswordHash, Salt, UserName) VALUES ('" + UserToSave.email + "','" + UserToSave.password + "','" + UserToSave.passwordHash + "','" + UserToSave.salt + "','" + UserToSave.username + "')";
            MySqlCommand cmd = new MySqlCommand(sqlString, conn);
            cmd.ExecuteNonQuery();
            long id = cmd.LastInsertedId;
            return id;
        }
    }
}